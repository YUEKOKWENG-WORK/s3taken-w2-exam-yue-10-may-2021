package bank.service;

public interface CustomerService {

	// insert customer record
	void insertCustomerRecords();

	// prompt user for a choice
	void customerChoice();

	// prompt user for login ID and password
	void customerLogin();

} // end of CustomerService interface

package bank.service;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.InputMismatchException;
import java.util.Scanner;

import bank.dao.CustomerDAO;
import bank.dao.CustomerDAOImplementation;
import bank.pojo.Customer;
import utility.DBUtility;

// CustomerServiceImplementation class container two method to prompt Customer Login ID and Password to login and
// prompt user for a choice of creating a new customer ID and Password or to logout.
public class CustomerServiceImplementation implements CustomerService {

	CustomerDAO refCustomerDAO = new CustomerDAOImplementation();;
	Scanner refScanner = new Scanner(System.in);
	Customer refCustomer;

	// prompt user for login ID and password and call a reference of Customer to set the Login ID and password
	@Override
	public void customerLogin() {
		System.out.print("Enter Login ID: ");
		String customerLoginID = refScanner.next();
		refScanner.nextLine();

		System.out.print("\nEnter Login Password: ");
		String customerPassword = refScanner.next();

		refCustomer = new Customer();
		refCustomer.setCustomerID(customerLoginID);
		refCustomer.setCustomerPassword(customerPassword);

		// calls the CustomerDAOImplementation boolean method of customerAuthentication to check with the Database is inside
		// passing in the reference of the Customer as parameter
		// if refCustomerDAO.customerAuthentication(refCustomer) returns false, prompt user for login again
		if (!refCustomerDAO.customerAuthentication(refCustomer)) {
			System.out.println("\nLogin Failed\n");
			customerLogin();
		}
		;
	} // end of customerLogin

	// prompt user for login ID and password and call a reference of Customer to set the Login ID and password
	@Override
	public void insertCustomerRecords() {

		System.out.print("Enter Login ID: ");
		String customerLoginID = refScanner.next();
		refScanner.nextLine();

		System.out.print("\nEnter Login Password: ");
		String customerPassword = refScanner.next();

		refCustomer = new Customer();
		refCustomer.setCustomerID(customerLoginID);
		refCustomer.setCustomerPassword(customerPassword);

		// calls the CustomerDAOImplementation class insertRecord method and pass in the Customer object reference
		// which takes in the user Login ID and password
		refCustomerDAO = new CustomerDAOImplementation();
		refCustomerDAO.insertRecord(refCustomer);

	} // end of userInputInsertRecord

	
	// Using a swithch case to prompt user for a choice of inserting a new user input or to logout of the application
	@Override
	public void customerChoice() {
		// try-catch user input mismatch if user enter a option other that number 1 or 2
		try {
			System.out.println("Option 1: Insert New Customer Details");
			System.out.println("Option 2: Logout");
			System.out.print("\nEnter Choice: ");
			refScanner = new Scanner(System.in);
			int choice = refScanner.nextInt();
			switch (choice) {
			case 1:
				insertCustomerRecords(); // calls insertCustomerRecords() implemented in line 46 
				customerChoice(); // prompt user for a choice again
				break;
			case 2:
				try {
					Connection refConnection = DBUtility.getConnection(); 
					refConnection.close(); // close db connection
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					System.out.println("\nClosing Connection..");
					System.out.println("\nSuccessfully Logout..");
				}
				System.exit(0); // close program without error
				break;
			default:
				System.out.println("\nOption not found..\n");
				customerChoice();
				break;
			}
		} catch (InputMismatchException e) {
			System.out.println("\nEnter Option 1 or 2\n");
			customerChoice();
		}
	} // end of customerChoice
} // end of CustomerServiceImplementation class

package bank.dao;

import bank.pojo.Customer;

public interface CustomerDAO {

	// insert a new record into the database
	void insertRecord(Customer refCustomer);

	// check if customer record is in the database
	boolean customerAuthentication(Customer refCustomer);

} // end of CustomerDAO interface

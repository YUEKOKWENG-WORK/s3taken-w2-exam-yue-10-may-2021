package bank.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import bank.dao.CustomerDAO;
import bank.pojo.Customer;
import utility.DBUtility;

public class CustomerDAOImplementation implements CustomerDAO {

	Connection refConnection = null;
	PreparedStatement refPreparedStatement = null;
	Statement refStatement = null;
	ResultSet refResultSet = null;
	private boolean authenticationStatus = false;

	// method takes in Customer reference and makes a connection to the database.
	// insert a new record into the database once connected
	@Override
	public void insertRecord(Customer refCustomer) {
		try {
			refConnection = DBUtility.getConnection();

			String sqlQuery = "insert into customer(customer_id, customer_password) values(?,?);";

			refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			refPreparedStatement.setString(1, refCustomer.getCustomerID());
			refPreparedStatement.setString(2, refCustomer.getCustomerPassword());
			refPreparedStatement.execute();
			System.out.println("\nNew Record Added..\n");

		} catch (Exception e) {
			System.out.println("\nException Handled while inserting record.\n");
		}
	} // end of insertRecord
	
	// method takes in Customer reference and makes a connection to the database.
	// makes a SQL query to the database to find a customer record that matches a record in the database
	// if same, boolean of authenticationStatus is set to true and return the boolean value to CustomerServiceImplementation - customerLogin();
	@Override
	public boolean customerAuthentication(Customer refCustomer) {
		try {
			refConnection = DBUtility.getConnection();

			String sqlQuery = "SELECT * FROM customer WHERE customer_id = ? AND customer_password = ?;";

			refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			refPreparedStatement.setString(1, refCustomer.getCustomerID());
			refPreparedStatement.setString(2, refCustomer.getCustomerPassword());
			refPreparedStatement.execute();

			refResultSet = refPreparedStatement.executeQuery();
			while (refResultSet.next()) {
				if (refResultSet.getString(1).equals(refCustomer.getCustomerID())
						&& refResultSet.getString(2).equals(refCustomer.getCustomerPassword())) {
					authenticationStatus = true;
					System.out.println("\nLogin Successful\n");
				} else {
					authenticationStatus = false;
				}
			}

		} catch (SQLException e) {
			System.out.println("\nException Handled while Authenticating Admin.\n");
		}
		return authenticationStatus;
	} // end of CustomerAuthentication
} // end of CustomerDAOImplementation class
